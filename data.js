window.schedulesData = [
  {
    id: 1,
    at: "2015-07-04T04:00:00.000Z",
    professional_id: 1,
    professional_name: "John"
  },
  {
    id: 2,
    at: "2015-07-04T05:00:00.000Z",
    professional_id: 1,
    professional_name: "John"
  },
  {
    id: 3,
    at: "2015-07-05T04:00:00.000Z",
    professional_id: 1,
    professional_name: "John"
  },
  {
    id: 4,
    at: "2015-07-06T08:00:00.000Z",
    professional_id: 1,
    professional_name: "John"
  },
  {
    id: 5,
    at: "2015-07-06T09:00:00.000Z",
    professional_id: 1,
    professional_name: "John"
  },
  {
    id: 6,
    at: "2015-07-04T06:00:00.000Z",
    professional_id: 2,
    professional_name: "Mary"
  },
  {
    id: 7,
    at: "2015-07-04T07:00:00.000Z",
    professional_id: 2,
    professional_name: "Mary"
  },
  {
    id: 8,
    at: "2015-07-05T05:00:00.000Z",
    professional_id: 2,
    professional_name: "Mary"
  },
  {
    id: 9,
    at: "2015-07-06T10:00:00.000Z",
    professional_id: 2,
    professional_name: "Mary"
  },
  {
    id: 10,
    at: "2015-07-07T04:00:00.000Z",
    professional_id: 3,
    professional_name: "Ruth"
  }
];
