window.Schedule = Fluxo.Store.extend({
  computed: {
    date: ["change:at"]
  },

  attributeParsers: {
    at: function (at) {
      return moment(at);
    }
  },

  date: function () {
    return this.data.at.clone().startOf("day");
  }
});
