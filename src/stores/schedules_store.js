window.Schedules = Fluxo.CollectionStore.extend({
  store: Schedule,

  computed: {
    professionals: ["add", "remove"],
    dates: ["change:selectedProfessionalId"],
    availableSchedules: ["change:selectedProfessionalId", "change:selectedDate"],
  },

  initialize: function () {
    this.setDate(this.data.dates[0]);
  },

  professionals: function () {
    var professionals = this.stores.map((store) => {
      return {
        id: store.data.professional_id,
        name: store.data.professional_name
      };
    });

    return _.uniq(professionals, (professional) => professional.id);
  },

  dates: function () {
    var criteria = {};

    if (this.data.selectedProfessionalId) {
      criteria.professional_id = this.data.selectedProfessionalId;
    }

    var dates = this.where(criteria).map((store) => store.data.date);

    return _.uniq(dates, (date) => date.valueOf());
  },

  availableSchedules: function () {
    return this.stores.filter((store) => {
      var match = true;

      if (this.data.selectedProfessionalId) {
        match = match && store.data.professional_id === this.data.selectedProfessionalId;
      }

      if (this.data.selectedDate) {
        match = match && store.data.date.isSame(this.data.selectedDate);
      }

      return match;
    });
  },

  setProfessional: function (professionalId) {
    this.setAttribute("selectedProfessionalId", professionalId);

    this.setDate(this.data.dates[0]);
  },

  setDate: function (date) {
    this.setAttribute("selectedDate", date);
  }
});
