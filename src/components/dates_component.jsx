class DatesComponent extends React.Component {
  renderDate (date) {
    var setDate = SchedulesActions.setDate.bind(undefined, date),
        isChecked = this.props.selectedDate.isSame(date),
        formatted = date.format("DD/MM/YYYY");

    return (
      <li key={date.valueOf()} className="list-group-item">
        <input checked={isChecked} name="date" onChange={setDate} type="radio" /> {formatted}
      </li>
    );
  }

  renderDates () {
    return this.props.dates.map(this.renderDate.bind(this));
  }

  render () {
    return (
       <div className="panel panel-primary">
        <div className="panel-heading">
          <h3 className="panel-title">Datas</h3>
        </div>

        <ul className="list-group">
          {this.renderDates()}
        </ul>
      </div>
    );
  }
}
