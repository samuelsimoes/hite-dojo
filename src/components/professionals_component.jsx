class ProfessionalsComponent extends React.Component {
  renderProfessional (professional) {
    var setProfessional = SchedulesActions.setProfessional.bind(undefined, professional.id),
        isChecked = professional.id === this.props.selectedProfessional;

    return (
      <li key={professional.id} className="list-group-item">
        <input
          checked={isChecked}
          name="professional"
          type="radio"
          onChange={setProfessional} /> {professional.name}
      </li>
    );
  }

  renderProfessionals () {
    return this.props.professionals.map(this.renderProfessional.bind(this));
  }

  render () {
    var noProfessional = !this.props.professionalId;

    return (
      <div className="panel panel-primary">
        <div className="panel-heading">
          <h3 className="panel-title">Profissionais</h3>
        </div>

        <ul className="list-group">
          {this.renderProfessional({id: undefined, name: "Qualquer Profissional"})}
          {this.renderProfessionals()}
        </ul>
      </div>
    );
  }
}
