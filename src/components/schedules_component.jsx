class SchedulesComponent extends React.Component {
  renderSchedules () {
    return this.props.schedules.map((store) => {
      return (
        <div key={store.data.id} className="list-group-item list-group-item-success clearfix">
          <p className="list-group-item-text pull-left">
            {moment(store.data.at).format("DD/MM/YYYY - HH:mm")}
            <br/>
            <small className="muted">{store.data.professional_name}</small>
          </p>
          <button type="button" className="btn btn-sm btn-success pull-right">
            Marcar
          </button>
        </div>
      );
    });
  }

  render () {
    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          Horários Disponíveis
        </div>

        <div className="list-group">
          {this.renderSchedules()}
        </div>
      </div>
    )
  }
}
