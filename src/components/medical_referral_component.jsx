class MedicalReferralComponent extends React.Component {
  render () {
    return (
      <div className="row">
        <div className="col-md-3">
          <ProfessionalsComponent
            professionals={this.props.schedules.data.professionals}
            selectedProfessional={this.props.schedules.data.selectedProfessionalId} />

          <DatesComponent
            dates={this.props.schedules.data.dates}
            selectedDate={this.props.schedules.data.selectedDate} />
        </div>

        <div className="col-md-9">
          <SchedulesComponent schedules={this.props.schedules.data.availableSchedules} />
        </div>
      </div>
    );
  }
}
