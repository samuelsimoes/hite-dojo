var schedules = new Schedules(window.schedulesData);

var MedicalReferralComponent =
    FluxoReactConnectStores(MedicalReferralComponent, { schedules: schedules });

React.render(
    React.createElement(MedicalReferralComponent),
    document.getElementById("app-container")
);
