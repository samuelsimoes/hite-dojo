var gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    babel = require('gulp-babel'),
    del = require('del');

gulp.task('clean', function (cb) {
  del('dist/*', cb);
});

gulp.task('build', function () {
  return gulp.src('src/**/*.js{,x}')
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist'));
});

gulp.task('watch', function () {
  gulp.watch('src/**/*.js{,x}', ['clean', 'build']);
});
